<?php
namespace small\tests;

class MoneyTest extends \PHPUnit\Framework\TestCase
{

    public function testCanBeNegated()
    {
        require_once('Money.php');
        // Arrange
        $a = new \small\Money(1);

        // Assert
        $this->assertEquals(4, $a->multiply());
    }
}

#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

# Install git (the php image doesn't have it) which is required by composer
apt-get update -yqq
apt-get install git -yqq

# Install xdebug
pecl install xdebug
docker-php-ext-enable xdebug


# Install phpunit, the tool that we will use for testing
curl --location --output /usr/local/bin/phpunit https://phar.phpunit.de/phpunit.phar
chmod +x /usr/local/bin/phpunit


# Install phpmd, the tool that we will use for testing
# curl --location --output /usr/local/bin/phpmd http://static.phpmd.org/php/latest/phpmd.phar
# chmod +x /usr/local/bin/phpmd


# Install phpmd, the tool that we will use for testing
curl --location --output /usr/local/bin/phpcs https://squizlabs.github.io/PHP_CodeSniffer/phpcs.phar
chmod +x /usr/local/bin/phpcs

# Install phpdox, for documenting the application
curl --location --output /usr/local/bin/phpdox http://phpdox.de/releases/phpdox.phar
chmod +x /usr/local/bin/phpdox

# Install mysql driver
# Here you can install any other extension that you need
docker-php-ext-install pdo_mysql

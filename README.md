# small
A very small repository for testing

This is set up so git-lab will pick up any changes and run the tests on it. It uses the .gitlab-ci.yml to set up environments to be able to test the project.

It will also trigger a build in the personal Jenkins at home that will also create documentation, and other things. It uses the build.xml file to be able to do all of that.
